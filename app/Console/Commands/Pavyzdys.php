<?php

namespace App\Console\Commands;

use App\Observer\CrawlerHandler;
use Illuminate\Console\Command;
use Spatie\Crawler\Crawler;

class Pavyzdys extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pavyzdys';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = 'https://www.delfi.lt/';
        Crawler::create()
            ->setCrawlObserver(new class extends CrawlerHandler {})
            ->startCrawling($url);
    }
}
